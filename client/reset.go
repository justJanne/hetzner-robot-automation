package client

import (
	"encoding/json"
	"fmt"
	"hetzner-robot/models"
	neturl "net/url"
)

func (c *Client) ResetGet(server models.ServerNumber) (*models.Reset, error) {
	url := fmt.Sprintf(c.baseURL+"/reset/%d", int(server))
	bytes, err := c.doGetRequest(url)
	if err != nil {
		return nil, err
	}

	var resetResp models.ResetResponse
	err = json.Unmarshal(bytes, &resetResp)
	if err != nil {
		return nil, err
	}

	return &resetResp.Reset, nil
}

func (c *Client) ResetSet(server models.ServerNumber, input *models.ResetSetInput) (*models.ResetPost, error) {
	url := fmt.Sprintf(c.baseURL+"/reset/%d", int(server))

	formData := neturl.Values{}
	formData.Set("type", input.Type)

	bytes, err := c.doPostFormRequest(url, formData)
	if err != nil {
		return nil, err
	}

	var resetResp models.ResetPostResponse
	err = json.Unmarshal(bytes, &resetResp)
	if err != nil {
		return nil, err
	}

	return &resetResp.Reset, nil
}
