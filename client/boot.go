package client

import (
	"encoding/json"
	"fmt"
	"hetzner-robot/models"
	"net/http"
	neturl "net/url"
	"strconv"
)

func (c *Client) BootRescueGet(server models.ServerNumber) (*models.RescueInfo, error) {
	url := fmt.Sprintf(c.baseURL+"/boot/%d/rescue", int(server))
	bytes, err := c.doGetRequest(url)
	if err != nil {
		return nil, err
	}

	var rescueResp models.RescueInfoGetResponse
	err = json.Unmarshal(bytes, &rescueResp)
	if err != nil {
		return nil, err
	}

	return &rescueResp.Rescue, nil
}

func (c *Client) BootRescueGetLast(server models.ServerNumber) (*models.Rescue, error) {
	url := fmt.Sprintf(c.baseURL+"/boot/%d/rescue/last", int(server))
	bytes, err := c.doGetRequest(url)
	if err != nil {
		return nil, err
	}

	var rescueResp models.RescueGetResponse
	err = json.Unmarshal(bytes, &rescueResp)
	if err != nil {
		return nil, err
	}

	return &rescueResp.Rescue, nil
}

func (c *Client) BootRescueSet(server models.ServerNumber, input *models.RescueSetInput) (*models.Rescue, error) {
	url := fmt.Sprintf(c.baseURL+"/boot/%d/rescue", int(server))

	formData := neturl.Values{}
	formData.Set("os", input.OS)
	if input.Arch > 0 {
		formData.Set("arch", strconv.Itoa(input.Arch))
	}
	if len(input.AuthorizedKeys) > 0 {
		for _, authorizedKey := range input.AuthorizedKeys {
			formData.Add("authorized_key[]", authorizedKey.Fingerprint)
		}
	}

	bytes, err := c.doPostFormRequest(url, formData)
	if err != nil {
		return nil, err
	}

	var rescueResp models.RescueGetResponse
	err = json.Unmarshal(bytes, &rescueResp)
	if err != nil {
		return nil, err
	}

	return &rescueResp.Rescue, nil
}

func (c *Client) BootRescueDelete(server models.ServerNumber) error {
	url := fmt.Sprintf(c.baseURL+"/boot/%d/rescue", int(server))

	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		return err
	}

	_, err = c.doRequest(req)
	return err
}
