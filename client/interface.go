package client

import (
	"hetzner-robot/models"
)

type RobotClient interface {
	SetBaseURL(baseURL string)
	SetUserAgent(userAgent string)
	GetVersion() string
	ServerGetList() ([]models.Server, error)
	ServerGet(server models.ServerNumber) (*models.Server, error)
	ServerSetName(server models.ServerNumber, input *models.ServerSetNameInput) (*models.Server, error)
	ServerReverse(server models.ServerNumber) (*models.Cancellation, error)
	KeyGetList() ([]models.Key, error)
	IPGetList() ([]models.IP, error)
	RDnsGetList() ([]models.Rdns, error)
	RDnsGet(server models.ServerNumber) (*models.Rdns, error)
	BootRescueGet(server models.ServerNumber) (*models.RescueInfo, error)
	BootRescueGetLast(server models.ServerNumber) (*models.Rescue, error)
	BootRescueSet(server models.ServerNumber, input *models.RescueSetInput) (*models.Rescue, error)
	BootRescueDelete(server models.ServerNumber) error
	ResetGet(server models.ServerNumber) (*models.Reset, error)
	ResetSet(server models.ServerNumber, input *models.ResetSetInput) (*models.ResetPost, error)
	FailoverGetList() ([]models.Failover, error)
	FailoverGet(server models.ServerNumber) (*models.Failover, error)
}
