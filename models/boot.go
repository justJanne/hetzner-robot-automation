package models

type RescueInfoGetResponse struct {
	Rescue RescueInfo `json:"rescue"`
}

type RescueGetResponse struct {
	Rescue Rescue `json:"rescue"`
}

type AuthorizedKey struct {
	Key Key `json:"key"`
}

type RescueInfo struct {
	ServerIP      string          `json:"server_ip"`
	ServerNumber  ServerNumber    `json:"server_number"`
	Os            []string        `json:"os"`
	Arch          []int           `json:"arch"`
	Active        bool            `json:"active"`
	Password      string          `json:"password"`
	AuthorizedKey []AuthorizedKey `json:"authorized_key"`
	HostKey       []interface{}   `json:"host_key"`
}

type Rescue struct {
	ServerIP      string          `json:"server_ip"`
	ServerNumber  ServerNumber    `json:"server_number"`
	Os            string          `json:"os"`
	Arch          int             `json:"arch"`
	Active        bool            `json:"active"`
	Password      string          `json:"password"`
	AuthorizedKey []AuthorizedKey `json:"authorized_key"`
	HostKey       []interface{}   `json:"host_key"`
}

type RescueSetInput struct {
	OS             string
	Arch           int
	AuthorizedKeys []Key
}
