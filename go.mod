module hetzner-robot-automation

go 1.18

require (
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
	golang.org/x/sync v0.0.0-20190423024810-112230192c58
)

require golang.org/x/sys v0.0.0-20220503163025-988cb79eb6c6 // indirect
