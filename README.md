## Usage

Invoke binary (hetzner-robot-automation) with a config file as parameter.
That config file should contain the API keys, server id, and list of files to upload and/or execute.

The binary then reboots the specified server into rescue mode, copies the specified files over, and executes them if
specified.

The config should comply with the JSON schema and look similar to

```json
{
  "api-key": "AzureDiamond",
  "api-secret": "hunter2",
  "server": 123456,
  "files": [
    {
      "source": "config/ignition.ign",
      "target": "/root/ignition.ign",
      "mode": 644,
      "execute": false
    },
    {
      "source": "install.sh",
      "target": "/root/install_rescue.sh",
      "mode": 755,
      "execute": true
    }
  ]
}
```