package main

import (
	"fmt"
	"hetzner-robot/client"
	"hetzner-robot/models"
	"log"
)

func enableRescue(robotClient client.RobotClient, server models.ServerNumber) (*SshConnection, error) {
	oldRescueInfo, err := robotClient.BootRescueGetLast(server)
	if err != nil {
		return nil, fmt.Errorf("error while retrieving rescue status: %w", err)
	}
	if oldRescueInfo.Active {
		log.Println("rescue system already active, deactivating")
		if err := robotClient.BootRescueDelete(server); err != nil {
			return nil, fmt.Errorf("error while deactivating rescue system: %w", err)
		}
	}

	log.Println("activating rescue system")
	rescueInfo, err := robotClient.BootRescueSet(server, &models.RescueSetInput{
		OS: "linux",
	})
	if err != nil {
		return nil, fmt.Errorf("error while setting server rescue properties: %w", err)
	}

	return &SshConnection{
		Username: "root",
		Password: rescueInfo.Password,
		Hostname: rescueInfo.ServerIP,
		Port:     22,
	}, nil
}

func provideConnection(manifest Manifest) (*SshConnection, error) {
	robotClient := client.NewBasicAuthClient(manifest.ApiKey, manifest.ApiSecret)

	server, err := robotClient.ServerGet(models.ServerNumber(manifest.Server))
	if err != nil {
		return nil, fmt.Errorf("error while retrieving server: %w", err)
	}
	connectionData, err := enableRescue(robotClient, server.ServerNumber)
	if err != nil {
		return nil, fmt.Errorf("error deploying configuration to server: %w", err)
	}
	log.Println("restarting server")
	_, err = robotClient.ResetSet(server.ServerNumber, &models.ResetSetInput{
		Type: models.ResetTypeHardware,
	})
	if err != nil {
		return nil, fmt.Errorf("error restarting server: %w", err)
	}
	return connectionData, nil
}
