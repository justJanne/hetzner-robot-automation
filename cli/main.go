package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

type Manifest struct {
	ApiKey    string     `json:"api-key"`
	ApiSecret string     `json:"api-secret"`
	Server    int        `json:"server"`
	Files     []CopyFile `json:"files"`
}

type CopyFile struct {
	Source  string `json:"source"`
	Target  string `json:"target"`
	Mode    uint   `json:"mode"`
	Execute bool   `json:"execute"`
}

func (manifest *Manifest) resolvePaths(root string) {
	for i, file := range manifest.Files {
		if !filepath.IsAbs(file.Source) {
			newPath := filepath.Join(root, file.Source)
			manifest.Files[i].Source = newPath
		}
	}
}

func loadManifest(filename string) (Manifest, error) {
	var manifest Manifest
	manifestPath, err := filepath.Abs(filename)
	if err != nil {
		return manifest, fmt.Errorf("could not find absolute path for file")
	}
	manifestFile, err := os.Open(manifestPath)
	if err != nil {
		return manifest, fmt.Errorf("error opening manifest: %w", err)
	}
	if err := json.NewDecoder(manifestFile).Decode(&manifest); err != nil {
		return manifest, fmt.Errorf("error loading manifest: %w", err)
	}
	manifest.resolvePaths(filepath.Dir(manifestPath))
	return manifest, nil
}

func main() {
	flag.Parse()
	for _, arg := range flag.Args() {
		log.Printf("loading manifest %s", arg)
		manifest, err := loadManifest(arg)
		if err != nil {
			log.Fatalf(err.Error())
		}
		err = deploy(manifest)
		if err != nil {
			log.Fatalf(err.Error())
		}
	}
}
